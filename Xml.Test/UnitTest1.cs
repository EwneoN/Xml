﻿using System;
using FluentAssertions;
using Xunit;

namespace Xml.Test
{
	public class UnitTest1
	{
		[Theory]
		[InlineData(false, false, false)]
		[InlineData(true, false, false)]
		[InlineData(true, true, false)]
		[InlineData(true, true, true)]
		[InlineData(false, true, true)]
		[InlineData(false, false, true)]
		[InlineData(false, true, false)]
		public void TestMethod1(bool allNull, bool allDefault, bool nullCollections)
		{
			TestClass1 class1 = new TestClass1(allNull, allDefault, nullCollections);

			string xml = null;

			Action action = () => xml = class1.SerialiseToXmlString();

			action.ShouldNotThrow();

			xml.Should().NotBeNullOrWhiteSpace();

			TestClass1 class1Copy = null;

			action = () => class1Copy = Serialisation.DeserialiseXmlString<TestClass1>(xml);

			action.ShouldNotThrow();

			class1Copy.Should().NotBeNull();

			class1Copy.ShouldBeEquivalentTo(class1);
		}
	}
}
