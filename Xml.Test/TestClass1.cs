﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace Xml.Test
{
	public class TestClass1 : IXmlSerialisable
	{
		#region Variables

		private short _Int16Value;
		private int _Int32Value;
		private long _Int64Value;

		private decimal _DecimalValue;
		private double _DoubleValue;
		private float _FloatValue;

		private bool _BooleanValue;
		private string _StringValue;

		private TestClass2[] _Array; 
		private List<TestClass2> _List;
		private Dictionary<int, Dictionary<int, TestClass2>> _DictionaryDictionary; 
		private Dictionary<int, TestClass2> _Dictionary; 

		#endregion

		#region Properties

		public short Int16Value
		{
			get { return _Int16Value; }
			set { _Int16Value = value; }
		}

		public int Int32Value
		{
			get { return _Int32Value; }
			set { _Int32Value = value; }
		}

		public long Int64Value
		{
			get { return _Int64Value; }
			set { _Int64Value = value; }
		}

		public decimal DecimalValue
		{
			get { return _DecimalValue; }
			set { _DecimalValue = value; }
		}

		public double DoubleValue
		{
			get { return _DoubleValue; }
			set { _DoubleValue = value; }
		}

		public float FloatValue
		{
			get { return _FloatValue; }
			set { _FloatValue = value; }
		}

		public bool BooleanValue
		{
			get { return _BooleanValue; }
			set { _BooleanValue = value; }
		}

		public string StringValue
		{
			get { return _StringValue; }
			set { _StringValue = value; }
		}

		public TestClass2[] Array
		{
			get { return _Array; }
			set { _Array = value; }
		}

		public List<TestClass2> List
		{
			get { return _List; }
			set { _List = value; }
		}

		public Dictionary<int, Dictionary<int, TestClass2>> DictionaryDictionary
		{
			get { return _DictionaryDictionary; }
			set { _DictionaryDictionary = value; }
		}

		public Dictionary<int, TestClass2> Dictionary
		{
			get { return _Dictionary; }
			set { _Dictionary = value; }
		}

		#endregion

		#region Constructors

		public TestClass1() { }

		public TestClass1(bool allNull = false, bool defaultValues = false, bool nullCollections = false)
		{
			if(!defaultValues)
			{
				_Int16Value = 1;
				_Int32Value = 2;
				_Int64Value = 3;

				_DecimalValue = 5.5M;
				_DoubleValue = 5.5;
				_FloatValue = 5.5F;

				_BooleanValue = true;
				_StringValue = "testy test test";
			}

			if (nullCollections)
			{
				return;
			}

			_Array = new[] { new TestClass2(allNull), new TestClass2(!allNull) };
			_List = new List<TestClass2> { new TestClass2(allNull), new TestClass2(!allNull) };
			_Dictionary = new Dictionary<int, TestClass2> { { 1, new TestClass2(allNull) }, { 2, new TestClass2(!allNull) } };

			_DictionaryDictionary = new Dictionary<int, Dictionary<int, TestClass2>>
			{
				{ 1, new Dictionary<int, TestClass2> { { 1, new TestClass2(allNull) }, { 2, new TestClass2(!allNull) } } }, 
				{ 2, new Dictionary<int, TestClass2> { { 1, new TestClass2(!allNull) }, { 2, new TestClass2(allNull) } } } 
			};
		}

		#endregion

		#region Public Methods

		public XmlSchema GetSchema()
		{
			return null;
		}

		public void ReadXml(XmlReader reader)
		{
			reader.ReadXml(this);
		}

		public void WriteXml(XmlWriter writer)
		{
			writer.WriteXml(this);
		}

		public void ReadXmlElement(XmlReader reader)
		{
			reader.ReadXmlIntoProperties(this);
		}

		public void WriteXmlElement(XmlWriter writer)
		{
			writer.WritePropertiesXml(this);
		}

		public XmlQualifiedName BuildXmlQualifiedName(XmlSchemaSet schemaSet)
		{
			return null;
		}

		#endregion
	}
}
