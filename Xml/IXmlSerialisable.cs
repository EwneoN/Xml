﻿using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Xml
{
	//[XmlSchemaProvider("BuildXmlQualifiedName")]
	public interface IXmlSerialisable : IXmlSerializable
	{
		void ReadXmlElement(XmlReader reader);

		void WriteXmlElement(XmlWriter writer);

		XmlQualifiedName BuildXmlQualifiedName(XmlSchemaSet schemaSet);
	}
}
