﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using Reflection;

namespace Xml
{
	public static class XmlReaderExtensions
	{
		public static void ReadXml(this XmlReader reader, IXmlSerialisable serialisable)
		{
			reader.MoveToContent();

			reader.ReadStartElement();

			if (reader.NodeType != XmlNodeType.EndElement)
			{
				serialisable.ReadXmlElement(reader);
			}

			if (reader.NodeType == XmlNodeType.EndElement)
			{
				reader.ReadEndElement();
			}
		}

		public static void ReadXmlIntoProperties(this XmlReader reader, IXmlSerialisable serialisable)
		{
			Type type = serialisable.GetType();

			while(reader.NodeType != XmlNodeType.EndElement)
			{
				if(reader.NodeType != XmlNodeType.Element)
				{
					continue;
				}
				
				PropertyInfo property = type.GetProperty(reader.Name);

				reader.ReadXmlIntoProperty(serialisable, property);
			}
		}

		public static void ReadXmlIntoProperty(this XmlReader reader, IXmlSerialisable serialisable, PropertyInfo property)
		{
			property.SetValue(serialisable, reader.ReadObjectXml(property.Name));
		}

		public static object ReadObjectXml(this XmlReader reader, string elementName = null)
		{
			if(reader.IsElementNull())
			{
				reader.Skip();
				return null;
			}

			Type type = reader.ReadTypeAttribute();

			if(type == null)
			{
				reader.Skip();
				return null;
			}

			if(type.GetInterfaces().Contains(typeof(IXmlSerialisable)))
			{
				IXmlSerialisable serialisable = Activator.CreateInstance(type) as IXmlSerialisable;

				if (serialisable == null)
				{
					return null;
				}

				serialisable.ReadXml(reader);

				return serialisable;
			}
			
			if (type.GetInterfaces().Contains(typeof(IList)) && type != typeof(Array))
			{
				IList objects = Activator.CreateInstance(type) as IList;

				if(objects == null)
				{
					reader.Skip();
					return null;
				}

				if (!reader.IsEmptyElement)
				{
					reader.ReadStartElement();

					while (reader.NodeType != XmlNodeType.EndElement)
					{
						if (reader.NodeType != XmlNodeType.Element)
						{
							continue;
						}

						object item = reader.ReadObjectXml();

						objects.Add(item);
					}

					reader.ReadEndElement();
				}
				else
				{
					reader.Read();
				}
				
				return objects;
			}
			
			if (type.GetInterfaces().Contains(typeof(IDictionary)))
			{
				IDictionary objects = Activator.CreateInstance(type) as IDictionary;

				if (objects == null)
				{
					reader.Skip();
					return null;
				}

				if(!reader.IsEmptyElement)
				{
					reader.ReadStartElement();

					while (reader.NodeType != XmlNodeType.EndElement)
					{
						if (reader.NodeType != XmlNodeType.Element)
						{
							continue;
						}

						object item = reader.ReadObjectXml();

						PropertyInfo keyProperty = item.GetType().GetProperty("Key");
						PropertyInfo valueProperty = item.GetType().GetProperty("Value");

						object kvpKey = keyProperty.GetValue(item);
						object kvpValue = valueProperty.GetValue(item);

						objects.Add(kvpKey, kvpValue);
					}

					reader.ReadEndElement();

					return Activator.CreateInstance(type, objects);
				}

				reader.Read();

				return Activator.CreateInstance(type);
			}

			if (type.IsArray || type == typeof(Array))
			{
				Type itemType = reader.ReadArrayItemTypeAttribute();

				Type listType = typeof(List<>).MakeGenericType(itemType);

				IList objects = Activator.CreateInstance(listType) as IList;

				if (objects == null)
				{
					reader.Skip();
					return null;
				}

				if (!reader.IsEmptyElement)
				{
					reader.ReadStartElement();

					while (reader.NodeType != XmlNodeType.EndElement)
					{
						if (reader.NodeType != XmlNodeType.Element)
						{
							continue;
						}

						object item = reader.ReadObjectXml();

						objects.Add(item);
					}

					reader.ReadEndElement();
				}
				else
				{
					reader.Read();
				}

				Array array = Array.CreateInstance(itemType, objects.Count);

				objects.CopyTo(array, 0); 

				return array;
			}

			// ReSharper disable once OperatorIsCanBeUsed
			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
			{
				//read item start
				reader.ReadStartElement();

				object key = reader.ReadObjectXml("Key");
				object value = reader.ReadObjectXml("Value");

				//read item end
				reader.ReadEndElement();

				return Activator.CreateInstance(type, key, value);
			}

			if (!type.IsValueType && type.GetConstructors().Any(c => !c.GetParameters().Any()))
			{
				return reader.ReadContentAs(type, new XmlNamespaceManager(new NameTable()));
			}

			object toReturn = reader.ReadElementAsType(elementName, type);

			return toReturn;
		}

		public static Type ReadTypeAttribute(this XmlReader reader)
		{
			string typeAttribute = reader.GetAttribute("type");

			return typeAttribute != null ? BuildType(typeAttribute) : null;
		}

		public static Type BuildType(string typeString)
		{
			if(String.IsNullOrWhiteSpace(typeString))
			{
				return null;
			}

			if (!typeString.Contains("["))
			{
				return Reflector.FindType(typeString);
			}

			Regex genericTypeRegex = new Regex(@"\A[^\[]*");
			string genericTypeString = genericTypeRegex.Match(typeString).Value;
			Type genericType = Reflector.FindType(genericTypeString);

			string typeParamsString = typeString.Substring(typeString.IndexOf("[") + 1);
			typeParamsString = typeParamsString.Remove(typeParamsString.LastIndexOf("]"));
			
			List<Type> typeParams = new List<Type>();

			string temp = typeParamsString;

			int depth = 0;
			int i = 0;

			while (i < temp.Length)
			{
				switch (temp[i])
				{
					case '[':
						depth++;
						break;
					case ']':
						depth--;
						break;
					case ',':
						if (depth == 0)
						{
							string typeParamString = temp.Substring(0, i);

							temp = temp.Substring(i + 1);

							i = -1;

							typeParams.Add(BuildType(typeParamString));
						}
						break;
				}

				i++;
			}

			typeParams.Add(BuildType(temp));

			return genericType.MakeGenericType(typeParams.ToArray());
		}

		public static Type ReadArrayItemTypeAttribute(this XmlReader reader)
		{
			string typeAttribute = reader.GetAttribute("arrayItemType");

			return Reflector.FindType(typeAttribute);
		}

		public static bool IsElementNull(this XmlReader reader)
		{
			string nilAttribute = reader.GetAttribute("nil");
			bool isNullValue;

			return (nilAttribute != null && Boolean.TryParse(nilAttribute, out isNullValue) && isNullValue);
		}

		public static void ReadNullElement(this XmlReader reader)
		{
			if (!reader.IsElementNull())
			{
				return;
			}

			switch (reader.NodeType)
			{
				case XmlNodeType.Element:
					reader.Skip();
					break;

				case XmlNodeType.EndElement:
					reader.ReadEndElement();
					break;

				default:
					reader.Read();
					break;
			}
		}

		public static string ReadElementAsString(this XmlReader reader, string name)
		{
			string value = null;

			if (!reader.IsElementNull())
			{
				value = reader.ReadElementContentAsString(name, reader.NamespaceURI);
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static T ReadElementAsObject<T>(this XmlReader reader)
			where T : IXmlSerializable
		{
			T value = default(T);

			if (!reader.IsElementNull())
			{
				Type typeToInstantiate = typeof (T);

				if (reader.HasAttributes)
				{
					string typeAttribute = reader.GetAttribute("Type");

					typeToInstantiate = Reflector.FindType(typeAttribute);
				}

				value = (T) Activator.CreateInstance(typeToInstantiate);
				value.ReadXml(reader);
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static object ReadElementAsObject(this XmlReader reader, Type type)
		{
			object value = default(object);

			if (!reader.IsElementNull())
			{
				Type typeToInstantiate = type;

				if (reader.HasAttributes)
				{
					string typeAttribute = reader.GetAttribute("Type");

					typeToInstantiate = Reflector.FindType(typeAttribute);
				}

				value = Activator.CreateInstance(typeToInstantiate);

				IXmlSerialisable serialisable = value as IXmlSerialisable;

				if (serialisable == null)
				{
					throw new InvalidOperationException("Type does not implement IXmlSerialisable");
				}

				serialisable.ReadXml(reader);
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static T ReadElementAsType<T>(this XmlReader reader, string name)
		{
			T value = default(T);

			if (!reader.IsElementNull())
			{
				Type type = typeof (T);

				if (type == typeof (bool))
				{
					return (T) (object) reader.ReadElementAsBoolean(name);
				}

				if (type == typeof (string))
				{
					return (T) (object) reader.ReadElementContentAsString(name, reader.NamespaceURI);
				}

				if (type == typeof (short))
				{
					return (T) (object) reader.ReadElementAsInt16(name);
				}

				if (type == typeof (int))
				{
					return (T) (object) reader.ReadElementAsInt32(name);
				}

				if (type == typeof (long))
				{
					return (T) (object) reader.ReadElementAsInt64(name);
				}

				if (type == typeof (decimal))
				{
					return (T) (object) reader.ReadElementAsDecimal(name);
				}

				if (type == typeof (double))
				{
					return (T) (object) reader.ReadElementAsDouble(name);
				}

				if (type == typeof (float))
				{
					return (T) (object) reader.ReadElementAsFloat(name);
				}

				if (type == typeof (char))
				{
					return (T) (object) reader.ReadElementAsChar(name);
				}

				if (type == typeof(DateTime))
				{
					return (T)(object)reader.ReadElementAsDateTime(name);
				}

				if (type == typeof(TimeSpan))
				{
					return (T)(object)reader.ReadElementAsTimeSpan(name);
				}

				if (type.IsEnum)
				{
					return (T) reader.ReadElementContentAsString(name, reader.NamespaceURI).ToEnum(type);
				}
			}

			switch (reader.NodeType)
			{
				case XmlNodeType.Element:
					reader.ReadStartElement();
					reader.ReadEndElement();
					break;

				case XmlNodeType.EndElement:
					reader.ReadEndElement();
					break;

				default:
					reader.Read();
					break;
			}

			return value;
		}

		public static object ReadElementAsType(this XmlReader reader, string name, Type type)
		{
			if (!reader.IsElementNull())
			{
				if (type == typeof(bool))
				{
					return reader.ReadElementAsBoolean(name);
				}

				if (type == typeof(string))
				{
					return reader.ReadElementContentAsString(name, reader.NamespaceURI);
				}

				if (type == typeof(short))
				{
					return reader.ReadElementAsInt16(name);
				}

				if (type == typeof(int))
				{
					return reader.ReadElementAsInt32(name);
				}

				if (type == typeof(long))
				{
					return reader.ReadElementAsInt64(name);
				}

				if (type == typeof(decimal))
				{
					return reader.ReadElementAsDecimal(name);
				}

				if (type == typeof(double))
				{
					return reader.ReadElementAsDouble(name);
				}

				if (type == typeof(float))
				{
					return reader.ReadElementAsFloat(name);
				}

				if (type == typeof(char))
				{
					return reader.ReadElementAsChar(name);
				}

				if (type == typeof(DateTime))
				{
					return reader.ReadElementAsDateTime(name);
				}

				if (type == typeof(TimeSpan))
				{
					return reader.ReadElementAsTimeSpan(name);
				}

				if (type.IsEnum)
				{
					return reader.ReadElementContentAsString(name, reader.NamespaceURI).ToEnum(type);
				}

				if (type == typeof(bool?))
				{
					return reader.ReadElementAsNullableBoolean(name);
				}

				if (type == typeof(short?))
				{
					return reader.ReadElementAsNullableInt16(name);
				}

				if (type == typeof(int?))
				{
					return reader.ReadElementAsNullableInt32(name);
				}

				if (type == typeof(long?))
				{
					return reader.ReadElementAsNullableInt64(name);
				}

				if (type == typeof(decimal?))
				{
					return reader.ReadElementAsNullableDecimal(name);
				}

				if (type == typeof(double?))
				{
					return reader.ReadElementAsNullableDouble(name);
				}

				if (type == typeof(float?))
				{
					return reader.ReadElementAsNullableFloat(name);
				}

				if (type == typeof(char?))
				{
					return reader.ReadElementAsNullableChar(name);
				}

				if (type == typeof(DateTime?))
				{
					return reader.ReadElementAsNullableDateTime(name);
				}

				if (type == typeof(TimeSpan?))
				{
					return reader.ReadElementAsNullableTimeSpan(name);
				}

				if (type.IsGenericParameter && type.GenericTypeArguments[0].IsEnum)
				{
					return reader.ReadElementContentAsString(name, reader.NamespaceURI).ToEnum(type);
				}
			}

			switch (reader.NodeType)
			{
				case XmlNodeType.Element:
					reader.ReadStartElement();

					while (reader.NodeType != XmlNodeType.EndElement)
					{
						reader.Read();
					}

					reader.ReadEndElement();
					break;

				case XmlNodeType.EndElement:
					reader.ReadEndElement();
					break;

				default:
					reader.Read();
					break;
			}

			return type.IsValueType ? Activator.CreateInstance(type) : null;
		}

		public static List<T> ReadElementAsObjectList<T>(this XmlReader reader)
			where T : IXmlSerializable
		{
			List<T> collection = new List<T>();

			if (!reader.IsElementNull() && !reader.IsEmptyElement)
			{
				reader.Read();

				while (reader.NodeType != XmlNodeType.EndElement)
				{
					Type typeToInstantiate = typeof (T);

					if (reader.HasAttributes)
					{
						string typeAttribute = reader.GetAttribute("Type");

						typeToInstantiate = Reflector.FindType(typeAttribute);
					}

					T item = (T) Activator.CreateInstance(typeToInstantiate);
					item.ReadXml(reader);

					collection.Add(item);
				}

				reader.Read();
			}
			else
			{
				reader.ReadNullElement();
			}

			return collection;
		}

		public static bool ReadElementAsBoolean(this XmlReader reader, string name, bool defaultValue = default(bool))
		{
			bool value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Boolean.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static short ReadElementAsInt16(this XmlReader reader, string name, short defaultValue = default(short))
		{
			short value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int16.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static int ReadElementAsInt32(this XmlReader reader, string name, int defaultValue = default(int))
		{
			int value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int32.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static long ReadElementAsInt64(this XmlReader reader, string name, long defaultValue = default(long))
		{
			long value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int64.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static decimal ReadElementAsDecimal(this XmlReader reader, string name, decimal defaultValue = default(decimal))
		{
			decimal value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Decimal.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static double ReadElementAsDouble(this XmlReader reader, string name, double defaultValue = default(double))
		{
			double value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Double.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static float ReadElementAsFloat(this XmlReader reader, string name, float defaultValue = default(float))
		{
			float value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Single.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static char ReadElementAsChar(this XmlReader reader, string name, char defaultValue = default(char))
		{
			char value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = reader.ReadElementContentAsString(name, reader.NamespaceURI)[0];
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static DateTime ReadElementAsDateTime(this XmlReader reader, string name, DateTime defaultValue = default(DateTime))
		{
			DateTime value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = DateTime.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static TimeSpan ReadElementAsTimeSpan(this XmlReader reader, string name, TimeSpan defaultValue = default(TimeSpan))
		{
			TimeSpan value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = TimeSpan.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static Guid ReadElementAsGuid(this XmlReader reader, string name, Guid defaultValue = default(Guid))
		{
			Guid value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Guid.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static TEnum ReadElementAsEnum<TEnum>(this XmlReader reader, string name, TEnum defaultValue = default(TEnum))
			where TEnum : struct, IComparable, IConvertible, IFormattable
		{
			TEnum value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = reader.ReadElementContentAsString(name, reader.NamespaceURI).ToEnum<TEnum>();
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static bool? ReadElementAsNullableBoolean(this XmlReader reader, string name, bool? defaultValue = default(bool?))
		{
			bool? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Boolean.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static short? ReadElementAsNullableInt16(this XmlReader reader, string name, short? defaultValue = default(short?))
		{
			short? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int16.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static int? ReadElementAsNullableInt32(this XmlReader reader, string name, int? defaultValue = default(int?))
		{
			int? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int32.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static long? ReadElementAsNullableInt64(this XmlReader reader, string name, long? defaultValue = default(long?))
		{
			long? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Int64.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static decimal? ReadElementAsNullableDecimal(this XmlReader reader, string name, decimal? defaultValue = default(decimal?))
		{
			decimal? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Decimal.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static double? ReadElementAsNullableDouble(this XmlReader reader, string name, double? defaultValue = default(double?))
		{
			double? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Double.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static float? ReadElementAsNullableFloat(this XmlReader reader, string name, float? defaultValue = default(float?))
		{
			float? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Single.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static char? ReadElementAsNullableChar(this XmlReader reader, string name, char? defaultValue = default(char?))
		{
			char? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = reader.ReadElementContentAsString(name, reader.NamespaceURI)[0];
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static DateTime? ReadElementAsNullableDateTime(this XmlReader reader, string name, DateTime? defaultValue = default(DateTime?))
		{
			DateTime? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = DateTime.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static TimeSpan? ReadElementAsNullableTimeSpan(this XmlReader reader, string name, TimeSpan? defaultValue = default(TimeSpan?))
		{
			TimeSpan? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = TimeSpan.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static Guid? ReadElementANullablesGuid(this XmlReader reader, string name, Guid? defaultValue = default(Guid?))
		{
			Guid? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = Guid.Parse(reader.ReadElementContentAsString(name, reader.NamespaceURI));
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}

		public static TEnum? ReadElementAsNullableEnum<TEnum>(this XmlReader reader, string name, TEnum? defaultValue = default(TEnum?))
			where TEnum : struct, IComparable, IConvertible, IFormattable
		{
			TEnum? value = defaultValue;

			if (!reader.IsElementNull())
			{
				value = reader.ReadElementContentAsString(name, reader.NamespaceURI).ToEnum<TEnum>();
			}
			else
			{
				reader.ReadNullElement();
			}

			return value;
		}
	}
}