﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using MoreLinq;

namespace Xml
{
	public static class XmlWriterExtensions
	{
		public static void WriteXml(this XmlWriter writer, IXmlSerialisable serialisable)
		{
			writer.WriteTypeAttribute(serialisable.GetType());

			serialisable.WriteXmlElement(writer);
		}

		public static void WritePropertiesXml(this XmlWriter writer, IXmlSerialisable serialisable)
		{
			serialisable.GetType().GetProperties().ForEach(prop => WritePropertyXml(writer, serialisable, prop));
		}

		public static void WritePropertyXml(this XmlWriter writer, IXmlSerialisable serialisable, PropertyInfo property)
		{
			if (property.GetCustomAttribute(typeof(XmlIgnoreAttribute)) != null)
			{
				return;
			}

			object value = property.GetValue(serialisable);

			writer.WriteObjectXml(value, property.PropertyType, property.Name);
		}

		public static void WriteObjectXml<T>(this XmlWriter writer, T obj, Type objectType, string elementName = null)
		{
			if(ReferenceEquals(obj, null))
			{
				if (objectType.IsArray)
				{
					writer.WriteNullArray(elementName, objectType.GetElementType());
					return;
				}

				if(String.IsNullOrWhiteSpace(elementName))
				{
					return;
				}

				writer.WriteNullElement(elementName, objectType);
				return;
			}

			if (obj is IXmlSerialisable)
			{
				writer.WriteObjectAsElement(elementName, obj as IXmlSerialisable);
			} 
			else if (objectType.IsValueType || objectType == typeof(string))
			{
				if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
				{
					PropertyInfo keyProperty = objectType.GetProperty("Key");
					PropertyInfo valueProperty = objectType.GetProperty("Value");

					object kvpKey = keyProperty.GetValue(obj);
					object kvpValue = valueProperty.GetValue(obj);

					writer.WriteStartElement(elementName ?? "KeyValuePair", String.Empty);
					writer.WriteTypeAttribute(typeof(KeyValuePair<,>).MakeGenericType(keyProperty.PropertyType, 
						valueProperty.PropertyType));

					writer.WriteObjectXml(kvpKey, keyProperty.PropertyType, "Key");

					writer.WriteObjectXml(kvpValue, valueProperty.PropertyType, "Value");

					writer.WriteEndElement();
				}
				else
				{
					if (elementName != null)
					{
						writer.WriteStartElement(elementName);
						writer.WriteTypeAttribute(objectType);

						writer.WriteValue(obj.ToString());

						writer.WriteEndElement();
					}
					else
					{
						writer.WriteValue(obj);
					}
				}
			}
			else if (obj is IEnumerable && (objectType.IsArray || obj is IList || obj is IDictionary))
			{
				writer.WriteStartElement(elementName ?? (objectType.Name + "s"), String.Empty);
				writer.WriteTypeAttribute(objectType.IsArray ? typeof(Array) : objectType);

				if (objectType.IsArray)
				{
					writer.WriteArrayItemTypeAttribute(objectType.GetElementType());

					foreach (object item in obj as IEnumerable)
					{
						writer.WriteObjectXml(item, objectType.GetElementType(), "Item");
					}
				}
				else if(obj is IList)
				{
					Type[] typeParams = objectType.GenericTypeArguments;

					foreach (object item in obj as IEnumerable)
					{
						writer.WriteObjectXml(item, typeParams[0], "Item");
					}
				}
				else
				{
					Type[] typeParams = objectType.GenericTypeArguments;

					foreach (object item in obj as IEnumerable)
					{
						writer.WriteObjectXml(item, typeof(KeyValuePair<,>).MakeGenericType(typeParams), "KeyValuePair");
					}
				}

				writer.WriteEndElement();
			}
			else if(objectType.GetConstructors().Any(c => !c.GetParameters().Any()))
			{
				writer.WriteStartElement(elementName ?? "Object");
				writer.WriteTypeAttribute(objectType);
				writer.WriteEndElement();
			}
		}

		public static void WriteNullAttribute(this XmlWriter writer)
		{
			writer.WriteAttributeString("nil", String.Empty, "true");
		}

		public static void WriteTypeAttribute(this XmlWriter writer, Type type)
		{
			writer.WriteAttributeString("type", String.Empty, type.BuildXmlTypeString());
		}
		public static void WriteArrayItemTypeAttribute(this XmlWriter writer, Type type)
		{
			writer.WriteAttributeString("arrayItemType", String.Empty, type.BuildXmlTypeString());
		}

		public static string BuildXmlTypeString(this Type type)
		{
			if (!type.IsGenericType)
			{
				return type.FullName;
			}

			Type genericType = type.GetGenericTypeDefinition();

			return String.Format("{0}[{1}]", genericType.FullName, String.Join(",", type.GenericTypeArguments
				.Select(BuildXmlTypeString)));
		}

		public static void WriteStringAsElement(this XmlWriter writer, string name, string value)
		{
			if (value != null)
			{
				writer.WriteElementString(name, value);
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteNullableElement<T>(this XmlWriter writer, string name, T value)
		{
			if (!ReferenceEquals(value, null))
			{
				writer.WriteElementString(name, value.ToString());
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteNullElement(this XmlWriter writer, string name)
		{
			writer.WriteStartElement(name, "");
			writer.WriteNullAttribute();
			writer.WriteEndElement();
		}

		public static void WriteNullElement(this XmlWriter writer, string name, Type type)
		{
			writer.WriteStartElement(name, "");
			writer.WriteNullAttribute();
			writer.WriteTypeAttribute(type.MakeArrayType());
			writer.WriteEndElement();
		}

		public static void WriteNullArray(this XmlWriter writer, string name, Type type)
		{
			writer.WriteStartElement(name, "");
			writer.WriteNullAttribute();
			writer.WriteTypeAttribute(typeof(Array));
			writer.WriteArrayItemTypeAttribute(type);
			writer.WriteEndElement();
		}

		public static void WriteObjectAsElement(this XmlWriter writer, string name, IXmlSerializable serialisable)
		{
			if (!ReferenceEquals(serialisable, null))
			{
				writer.WriteStartElement(name);

				serialisable.WriteXml(writer);

				writer.WriteEndElement();
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteObjectCollectionAsElement(this XmlWriter writer, string collectionName,
																											string itemName, ICollection<IXmlSerializable> collection)
		{
			if (!ReferenceEquals(collection, null))
			{
				writer.WriteStartElement(collectionName);

				collection.ForEach(serialisable => writer.WriteObjectAsElement(itemName, serialisable));

				writer.WriteEndElement();
			}
			else
			{
				writer.WriteNullElement(collectionName);
			}
		}

		public static void WriteCharAsElement(this XmlWriter writer, string name, char value)
		{
			if (value != default(char))
			{
				writer.WriteElementString(name, value.ToString());
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteCharAsElement(this XmlWriter writer, string name, char? value)
    {
      if (value.HasValue)
      {
        writer.WriteCharAsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

	  public static void WriteGuidAsElement(this XmlWriter writer, string name, Guid value)
	  {
      writer.WriteElementString(name, value.ToString());
	  }

    public static void WriteGuidAsElement(this XmlWriter writer, string name, Guid? value)
    {
      if (value.HasValue)
      {
        writer.WriteGuidAsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteDecimalAsElement(this XmlWriter writer, string name, decimal value)
    {
      if (value != default(decimal))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteDecimalAsElement(this XmlWriter writer, string name, decimal? value)
    {
      if (value.HasValue)
      {
        writer.WriteDecimalAsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt16AsElement(this XmlWriter writer, string name, short value)
    {
      if (value != default(short))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt16AsElement(this XmlWriter writer, string name, short? value)
    {
      if (value.HasValue)
      {
        writer.WriteInt16AsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt32AsElement(this XmlWriter writer, string name, int value)
    {
      if (value != default(int))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt32AsElement(this XmlWriter writer, string name, int? value)
    {
      if (value.HasValue)
      {
        writer.WriteInt32AsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt64AsElement(this XmlWriter writer, string name, long value)
    {
      if (value != default(long))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteInt64AsElement(this XmlWriter writer, string name, long? value)
    {
      if (value.HasValue)
      {
        writer.WriteInt64AsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteDoubleAsElement(this XmlWriter writer, string name, double value)
    {
			if (value > default(double) || value < default(double))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteDoubleAsElement(this XmlWriter writer, string name, double? value)
    {
      if (value.HasValue)
      {
        writer.WriteDoubleAsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteFloatAsElement(this XmlWriter writer, string name, float value)
    {
      if (value > default(float) || value < default(float))
      {
        writer.WriteElementString(name, value.ToString());
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

    public static void WriteFloatAsElement(this XmlWriter writer, string name, float? value)
    {
      if (value.HasValue)
      {
        writer.WriteFloatAsElement(name, value.Value);
      }
      else
      {
        writer.WriteNullElement(name);
      }
    }

		public static void WriteBoolAsElement(this XmlWriter writer, string name, bool value)
		{
			writer.WriteElementString(name, value.ToString());
		}

		public static void WriteBoolAsElement(this XmlWriter writer, string name, bool? value)
		{
			if (value.HasValue)
			{
				writer.WriteBoolAsElement(name, value.Value);
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteEnumAsElement(this XmlWriter writer, string name, Enum value)
		{
			if (value != null)
			{
				writer.WriteElementString(name, value.ToString());
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteDateTimeAsElement(this XmlWriter writer, string name, DateTime dateTime)
		{
			writer.WriteElementString(name, dateTime.ToString());
		}

		public static void WriteDateTimeAsElement(this XmlWriter writer, string name, DateTime? dateTime)
		{
			if (dateTime.HasValue)
			{
				writer.WriteDateTimeAsElement(name, (DateTime)dateTime);
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}

		public static void WriteTimeSpanAsElement(this XmlWriter writer, string name, TimeSpan timeSpan)
		{
			writer.WriteElementString(name, timeSpan.ToString());
		}

		public static void WriteTimeSpanAsElement(this XmlWriter writer, string name, TimeSpan? timeSpan)
		{
			if (timeSpan.HasValue)
			{
				writer.WriteTimeSpanAsElement(name, (TimeSpan)timeSpan);
			}
			else
			{
				writer.WriteNullElement(name);
			}
		}
	}
}
