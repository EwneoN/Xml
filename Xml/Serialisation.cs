﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using System.Xml.Serialization;
using Reflection;

namespace Xml
{
	public static class Serialisation
	{
		#region Constants

		public const string NAMESPACE = "http://heliosfear.enterprises.com/Xml";
		public const string XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";

		#endregion

		#region Variables

		private static readonly Dictionary<Type, XmlSerializer> _SerializerCache;
		private static readonly Dictionary<Type, XmlSerializer> _ArraySerializerCache;
		private static readonly Dictionary<Type, XmlTypeCode[]> _XmlTypeMapping;

		private static readonly object _SerializerCacheLock;
		private static readonly object _ArraySerializerCacheLock;
		private static readonly object _XmlTypeMappingLock;

		#endregion

		#region Properties

		public static Dictionary<Type, XmlSerializer> SerializerCache
		{
			get { lock (_SerializerCacheLock) { return _SerializerCache; } }
		}

		public static Dictionary<Type, XmlSerializer> ArraySerializerCache
		{
			get { lock (_ArraySerializerCacheLock) { return _ArraySerializerCache; } }
		}

		public static Dictionary<Type, XmlTypeCode[]> XmlTypeMapping
		{
			get { lock (_XmlTypeMappingLock) { return _XmlTypeMapping; } }
		}

		#endregion

		#region Constructors

		static Serialisation()
		{
			_SerializerCacheLock = new object();
			_ArraySerializerCacheLock = new object();
			_XmlTypeMappingLock = new object();

			_SerializerCache = new Dictionary<Type, XmlSerializer>();
			_ArraySerializerCache = new Dictionary<Type, XmlSerializer>();

			Dictionary<Type, XmlTypeCode[]> dictionary = Enum.GetValues(typeof (XmlTypeCode))
				.OfType<XmlTypeCode>()
				.Select(cc => new { cc, xt = XmlSchemaType.GetBuiltInSimpleType(cc) })
				.Where(t => t.xt != null)
				.GroupBy(t => t.xt.Datatype.ValueType, t => t.cc)
				.Select(gg => new { Type = gg.Key, XmlTypeCodes = gg.ToArray() })
				.ToDictionary(t => t.Type, t => t.XmlTypeCodes);
			_XmlTypeMapping = dictionary;
		}

		#endregion

		#region Public Methods

		public static XmlSerializer GetXmlSerializer(Type type)
		{
			XmlSerializer serializer;

			lock (_SerializerCacheLock)
			{
				if (_SerializerCache.ContainsKey(type))
				{
					serializer = _SerializerCache[type];
				}
				else
				{
					Type[] types = AppDomain.CurrentDomain.GetAssemblies()
						.SelectMany(a => a.GetTypes())
						.Where(t => type.IsInterface ? t.GetInterfaces().Contains(type) : t.IsSubclassOf(type))
						.ToArray();

					serializer = new XmlSerializer(type, types);

					_SerializerCache.Add(type, serializer);
				}
			}

			return serializer;
		}

		public static XmlSerializer GetXmlArraySerializer(Type type)
		{
			XmlSerializer serializer;

			lock(_ArraySerializerCacheLock)
			{
				if (_ArraySerializerCache.ContainsKey(type))
				{
					serializer = _ArraySerializerCache[type];
				}
				else
				{
					Type[] types = AppDomain.CurrentDomain.GetAssemblies()
						.SelectMany(a => a.GetTypes())
						.Where(t => type.IsInterface ? t.GetInterfaces().Contains(type) : t.IsSubclassOf(type))
						.Select(t => t.MakeArrayType())
						.ToArray();

					serializer = new XmlSerializer(type, types);

					_ArraySerializerCache.Add(type, serializer);
				}
			}

			return serializer;
		}

		public static string SerialiseToXmlString<T>(this T toSerialise)
		{
			string toReturn;

			using (StringWriter writer = new StringWriter())
			{
				SerialiseToXml(toSerialise, writer);

				toReturn = writer.ToString();
				writer.Flush();
				writer.Close();
			}

			return toReturn;
		}

		public static string SerialiseToXmlString<T>(this List<T> toSerialise)
		{
			string toReturn;

			using (StringWriter writer = new StringWriter())
			{
				SerialiseToXml(toSerialise.ToArray(), writer);

				toReturn = writer.ToString();
				writer.Flush();
				writer.Close();
			}

			return toReturn;
		}

		public static string SerialiseToXmlString<T>(this T[] toSerialise)
		{
			string toReturn;

			using (StringWriter writer = new StringWriter())
			{
				SerialiseToXml(toSerialise, writer);

				toReturn = writer.ToString();
				writer.Flush();
				writer.Close();
			}

			return toReturn;
		}

		public static void SerialiseToXmlFile<T>(this T toSerialise, FileInfo file)
		{
			toSerialise.SerialiseToXmlFile(file.FullName);
		}

		public static void SerialiseToXmlFile<T>(this T toSerialise, string filePath)
		{
			using (StreamWriter writer = new StreamWriter(filePath))
			{
				SerialiseToXml(toSerialise, writer);
				writer.Flush();
				writer.Close();
			}
		}

		public static void SerialiseToXmlFile<T>(this List<T> toSerialise, string filePath)
		{
			using (StreamWriter writer = new StreamWriter(filePath))
			{
				SerialiseToXml(toSerialise.ToArray(), writer);
				writer.Flush();
				writer.Close();
			}
		}

		public static void SerialiseToXmlFile<T>(this T[] toSerialise, string filePath)
		{
			using (StreamWriter writer = new StreamWriter(filePath))
			{
				SerialiseToXml(toSerialise, writer);
				writer.Flush();
				writer.Close();
			}
		}

		public static void SerialiseToXml<T>(this T objectToSerialize, TextWriter writer)
		{
			Type type = objectToSerialize.GetType();
			XmlSerializer serializer = GetXmlSerializer(type);

			serializer.Serialize(writer, objectToSerialize);
		}

		public static void SerialiseToXml<T>(this T[] objectToSerialize, TextWriter writer)
		{
			Type type = objectToSerialize.GetType();
			XmlSerializer serializer = GetXmlArraySerializer(type);

			serializer.Serialize(writer, objectToSerialize);
		}

		public static T DeserialiseXmlString<T>(string xmlString)
		{
			T toReturn;

			using (StringReader reader = new StringReader(xmlString))
			{
				toReturn = DeserialiseXml<T>(reader);
				reader.Close();
			}

			return toReturn;
		}

		public static T DeserialiseXmlArrayString<T>(string xmlString)
		{
			T toReturn;

			using (StringReader reader = new StringReader(xmlString))
			{
				toReturn = DeserialiseXmlArray<T>(reader);
				reader.Close();
			}

			return toReturn;
		}

		public static T DeserialiseXmlFile<T>(FileInfo file)
		{
			return DeserialiseXmlFile<T>(file.FullName);
		}

		public static T DeserialiseXmlFile<T>(string filePath)
		{
			T toReturn;

			using (StreamReader reader = new StreamReader(filePath))
			{
				toReturn = DeserialiseXml<T>(reader);
				reader.Close();
			}

			return toReturn;
		}

		public static T DeserialiseXmlArrayFile<T>(FileInfo file)
		{
			return DeserialiseXmlArrayFile<T>(file.FullName);
		}

		public static T DeserialiseXmlArrayFile<T>(string filePath)
		{
			T toReturn;

			using (StreamReader reader = new StreamReader(filePath))
			{
				toReturn = DeserialiseXmlArray<T>(reader);
				reader.Close();
			}

			return toReturn;
		}

		public static T DeserialiseXml<T>(TextReader reader)
		{
			Type type;
			XmlSerializer serializer;

			string lines = reader.ReadToEnd();

			if (String.IsNullOrWhiteSpace(lines))
			{
				return default(T);
			}

			Regex regex = new Regex("(?<= type=\").+(?=\")");
			Match match = regex.Match(lines);

			if (match.Success)
			{
				type = Reflector.FindType(match.Value) ?? typeof(T);
				serializer = GetXmlSerializer(type);

				using (StringReader stringReader = new StringReader(lines))
				{
					T value = (T)serializer.Deserialize(stringReader);
					stringReader.Close();
					return value;
				}
			}

			type = typeof(T);

			serializer = GetXmlSerializer(type);

			using (StringReader stringReader = new StringReader(lines))
			{
				T value = (T)serializer.Deserialize(stringReader);
				stringReader.Close();
				return value;
			}
		}

		public static T DeserialiseXmlArray<T>(TextReader reader)
		{
			Type type;
			XmlSerializer serializer;

			string lines = reader.ReadToEnd();

			if (String.IsNullOrWhiteSpace(lines))
			{
				return default(T);
			}

			Regex regex = new Regex("(?<= type=\").+(?=\")");
			Match match = regex.Match(lines);

			if (match.Success)
			{
				type = Reflector.FindType(match.Value) != null ?
					Reflector.FindType(match.Value).MakeArrayType() : typeof(T);

				serializer = GetXmlArraySerializer(type);

				using (TextReader stringReader = new StringReader(lines))
				{
          T array = (T)serializer.Deserialize(stringReader);
          stringReader.Close();
					return array;
				}
			}

			type = typeof(T);
			serializer = GetXmlArraySerializer(type);

			using (StringReader stringReader = new StringReader(lines))
			{
				T array = (T)serializer.Deserialize(reader);
				stringReader.Close();
				return array;
			}
		}

		public static string ContractSerialiseToXmlString<T>(this T objectToSerialize)
		{
			string toReturn;
			MemoryStream stream = new MemoryStream();
			DataContractSerializer serialiser = new DataContractSerializer(typeof(T));

			serialiser.WriteObject(stream, objectToSerialize);

			stream.Position = 0;

			using (StreamReader reader = new StreamReader(stream))
			{
				toReturn = reader.ReadToEnd();
				reader.Close();
			}

			return toReturn;
		}

		public static void ContractSerialiseToXmlFile<T>(this T objectToSerialize, string filePath)
		{
			using (FileStream writer = new FileStream(filePath, FileMode.Create))
			{
				DataContractSerializer serialiser = new DataContractSerializer(typeof(T));

				serialiser.WriteObject(writer, objectToSerialize);
				writer.Flush();
				writer.Close();
			}
		}

		public static T ContractDeserialiseXmlFile<T>(string filePath)
			where T : class
		{
			T toReturn;

			using (Stream stream = new FileStream(filePath, FileMode.Open))
			{
				DataContractSerializer serialiser = new DataContractSerializer(typeof(T));

				toReturn = serialiser.ReadObject(stream) as T;
				stream.Flush();
				stream.Close();
			}

			return toReturn;
		}

		/// <summary>
		/// Takes the passed in string and attempts to deserialise and create an
		/// instance of T.
		/// </summary>
		/// <typeparam name="T">The type of class we are deserialising.</typeparam>
		/// <param name="xmlString">A string containing the XML to deserialise.</param>
		/// <returns>An instance of T desrialised from the passed in XML string.</returns>
		public static T ContractDeserialiseXmlString<T>(string xmlString)
			where T : class
		{
			T toReturn;

			byte[] bytes = Encoding.UTF8.GetBytes(xmlString);

			using (Stream stream = new MemoryStream(bytes))
			{
				DataContractSerializer serialiser = new DataContractSerializer(typeof(T));

				toReturn = serialiser.ReadObject(stream) as T;
				stream.Close();
			}

			return toReturn;
		}

		#endregion

		#region Internal Methods

		internal static T ToEnum<T>(this string enumString)
			where T : struct, IComparable, IConvertible, IFormattable
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enum type");
			}

			T toReturn;

			try
			{
				toReturn = (T)Enum.Parse(typeof(T), enumString, true);
			}
			catch (Exception ex)
			{
				const string format = "Failed to convert string to {0}. See inner exception for details";

				throw new InvalidOperationException(String.Format(format, typeof(T).FullName), ex);
			}

			return toReturn;
		}

		internal static object ToEnum(this string enumString, Type type)
		{
			if (!type.IsEnum)
			{
				throw new ArgumentException("T must be an enum type");
			}

			object toReturn;

			try
			{
				toReturn = Enum.Parse(type, enumString, true);
			}
			catch (Exception ex)
			{
				const string format = "Failed to convert string to {0}. See inner exception for details";

				throw new InvalidOperationException(String.Format(format, type.FullName), ex);
			}

			return toReturn;
		}

		#endregion
	}
}
